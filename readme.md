# TypeScript Express API 


## Running the app

```
# install dependencies
npm install

# run in dev mode on port 4000
npm run dev

# generate production build
npm run build

# run generated content for prod
npm run start
```
