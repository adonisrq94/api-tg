import { Router } from 'express';

import UserController from '../controllers/UserController';


class UserRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    routes() {
        this.router.get('/', UserController.getUsers);
        this.router.get('/:id', UserController.getUser);
        this.router.post('/', UserController.createUser);
        this.router.put('/:id', UserController.updateUser);
        this.router.delete('/:id', UserController.deleteUser);
    }

}

const userRouter = new UserRouter();
export default userRouter.router;

