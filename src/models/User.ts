import { Schema, model, Document } from "mongoose";

const UserSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  lastname: { type: String, required: true ,lowercase: true},
  phone: { type: Number, required: true },
});


export interface IUser extends Document{
  name: string;
  email: string;
  lastname: string;
  phone: number;
}

export default model<IUser>("User", UserSchema);
