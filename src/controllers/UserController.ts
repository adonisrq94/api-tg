import { Request, Response } from 'express';

import UsersService from '../services/users';



class UserController {


    constructor() {

    }

    public async getUsers(req: Request, res: Response ): Promise<void>  {
        try {
            const response = await UsersService.getUsers()
            if (response.length > 0) {
                res.status(200).json({
                    data: response,
                    message: 'list users'
                });
            } else {
                res.status(404).json({
                    message: 'not found'
                });
            }

        } catch (error) {
            res.status(500).json({
                error: error,
            });
        }
    }

    async getUser(req: Request, res: Response ): Promise<void> {
        try {
            const user = await UsersService.getUser(req.params.id);
            if (user.length > 0) {
                res.status(200).json({
                    data: user,
                    message: 'get user'
                });
            } else {
                res.status(404).json({
                    message: 'not found'
                });
            }
        } catch (error) {
            res.status(500).json({
                error: error,
            });
        }
    }

    async createUser(req: Request, res: Response ): Promise<void> {
        try {
            const newUser = await UsersService.createUser(req.body);
            res.status(201).json({
                data: newUser,
                message: 'user created'
            });
        } catch (error) {
            res.status(500).json({
                error: error,
            });        
        }
    }

    async updateUser(req: Request, res: Response ): Promise<void> {
        try {
            const { id } = req.params;
            console.log("id",id)
            const user = await UsersService.updateUser(id, req.body);        
            res.status(200).json({
                data: user,
                message: 'user update'
            });     
        } catch (error) {
            res.status(500).json({
                error: error,
            });
        }

    }

    async deleteUser(req: Request, res: Response ): Promise<void> {
        try {
            const { id } = req.params;
            const user = await UsersService.deleteUser(id);        
            res.status(200).json({
                data: user,
                message: 'user delete'
            });    
        } catch (error) {
            res.status(500).json({
                error: error,
            });
        }
    }
}

const controller = new UserController()

export default controller;


