
import User, { IUser } from '../models/User';

class UsersService {

    constructor() {}

    async getUsers():Promise<any>{
        const users = await User.find();
        return users;
    }

    async getUser(id: string): Promise<any> {
        const user = await User.findById(id);
        return user;
    }

    async createUser(data: any): Promise<any> {
        const newUser: IUser = new User(data);
        await newUser.save();   
        return newUser;        
    }
    
    async updateUser(id:string, data:any): Promise<any> {
        const user = await User.findByIdAndUpdate(id, data, {new: true});
        return user;
    }

    async deleteUser(id:string): Promise<any> {
        const user = await User.findByIdAndRemove(id);
        return user;
    }

}

const service = new UsersService()

export default service;

