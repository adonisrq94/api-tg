import mongoose from "mongoose";


class mongoConnect {
    public USER: string;
    public PASSWORD: string;
    public DB_NAME: string;
    public HOST: string;

    constructor() {
        this.USER = encodeURIComponent('db_user_tg');
        this.PASSWORD = encodeURIComponent('mongo2020');
        this.DB_NAME = 'tg_db';
        this.HOST = encodeURIComponent('cluster0.1en8y.mongodb.net');
    }

    public getUrl():string {
        const MONGO_URI = process.env.MONGODB_URL ? process.env.MONGODB_URL:
        `mongodb+srv://${this.USER}:${this.PASSWORD}@${this.HOST}/${this.DB_NAME}?retryWrites=true&w=majority`;
        return MONGO_URI;
    }

    public start(): void {
        mongoose.connect(this.getUrl())
        .then(db => console.log('db is connected'))
        .catch(e => console.log('Error [mongo] - ',e))
    }

}

const service = new mongoConnect()

export default service;

